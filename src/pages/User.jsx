import React, { useEffect, useState } from "react";
import UserFrom from "../components/_user/user-form";
import axios from "axios";
import fetchData from "../components/fetch_data";
export default function Users() {
  const [users, setUsers] = useState([]);
  const [user, setUser] = useState(null);

  const [forForUpdate, setForForUpdate] = useState(false);

  const [login, setLogin] = useState();
  const [avatar_url, setavatar_url] = useState();
  const [github_url, setgithub_url] = useState();

  function view() {
    fetchData().then((data) => {
      setUsers(data);
    });
  }

  function clearForm() {
    setLogin("");
    setavatar_url("");
    setgithub_url("");
  }

  const create = (e) => {
    e.preventDefault();
    const data = {
      id: fetchData().length,
      login,
      avatar_url,
      github_url,
    };
    // Add the user to the json server
    axios
      .post("http://localhost:8000/users", data, {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      })
      .then((resp) => {
        console.log(resp.data);
        view();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  function update(e) {
    e.preventDefault();

    const data = {
      login,
      avatar_url,
      github_url,
    };
    axios
      .put(`http://localhost:8000/users/${user.id}`, data)
      .then((resp) => {
        console.log(resp.data);
        view();
      })
      .catch((error) => {
        console.log(error);
      });
    setForForUpdate(false);
    clearForm();
  }

  const remove = (id) => {
    // Add the user to the json server
    axios
      .delete("http://localhost:8000/users/" + id)
      .then((resp) => {
        console.log(resp.data);
        view();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  function edit(user) {
    setForForUpdate(true);
    setLogin(user.login);
    setavatar_url(user.avatar_url);
    setgithub_url(user.github_url);
    setUser(user);
  }
  useEffect(() => {
    view();
  }, []);

  return (
    <>
      <div className="container">
        <div className="mt-5 mb-5">
          <div className="container">
            <form
              onSubmit={(e) => (forForUpdate ? update(e) : create(e))}
              action="http://localhost:8000/users"
            >
              <div className="mb-3">
                <label htmlFor="login" className="form-label">
                  Login
                </label>
                <input
                  type="text"
                  className="form-control"
                  id="login"
                  name="login"
                  onChange={(e) => setLogin(e.target.value)}
                  value={login}
                  required
                />
              </div>
              <div className="mb-3">
                <label htmlFor="avatar" className="form-label">
                  avatar
                </label>
                <input
                  type="text"
                  className="form-control"
                  id="avatar"
                  name="avatar_url"
                  onChange={(e) => setavatar_url(e.target.value)}
                  value={avatar_url}
                  required
                />
              </div>
              <div className="mb-3">
                <label htmlFor="avatar" className="form-label">
                  Github url
                </label>
                <input
                  type="text"
                  className="form-control"
                  id="github"
                  name="github_url"
                  onChange={(e) => setgithub_url(e.target.value)}
                  value={github_url}
                  required
                />
              </div>
              <div className="mb-3">
                <input
                  type="submit"
                  className="btn btn-primary"
                  value={forForUpdate ? "Update" : "Save"}
                />
              </div>
            </form>
          </div>
        </div>

        <div className="row row-cols-1 row-cols-sm-2 row-cols-md-3 row-cols-lg-4 g-4">
          {users.map((user) => (
            <div className="col">
              <div className="card">
                <a href={user.html_url}>
                  <img
                    src={user.avatar_url}
                    className="card-img-top"
                    alt="..."
                  />
                </a>
                <div className="card-body">
                  <h5 className="card-title">{user.login}</h5>
                  <p className="card-text">
                    Some quick example text to build on the card title and make
                    up the bulk of the card's content.
                  </p>

                  <button
                    className="btn btn-danger me-2"
                    onClick={() => remove(user.id)}
                  >
                    delete
                  </button>
                  <button
                    className="btn btn-primary"
                    onClick={() => edit(user)}
                  >
                    edit
                  </button>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </>
  );
}
